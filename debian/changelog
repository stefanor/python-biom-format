python-biom-format (2.1.10-2) unstable; urgency=medium

  * Fix watchfile to detect new versions on github
  * Standards-Version: 4.6.0 (routine-update)
  * Apply multi-arch hints.
    + python-biom-format-doc: Add Multi-Arch: foreign.
  * add_javascript() was deprecated in v1.8 and removed in v4.0. add_js_file()
    is a successor of the API
    Closes: #997377
  * Ignore failing hdf5 test

 -- Andreas Tille <tille@debian.org>  Mon, 01 Nov 2021 13:40:36 +0100

python-biom-format (2.1.10-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 21 Nov 2020 18:07:53 +0530

python-biom-format (2.1.9-2) unstable; urgency=medium

  * Team upload.
  * remove .coverage files. Closes: #974050

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 10 Nov 2020 10:43:29 +0100

python-biom-format (2.1.9-1) unstable; urgency=medium

  * Team Upload.

  [ Andreas Tille ]
  * New upstream version
    Closes: #950929
  * Build-Depends: python3-pytest

  [ Michael R. Crusoe ]
  * Build-Depends: python3-pytest-cov

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 06 Nov 2020 11:23:23 +0100

python-biom-format (2.1.8+dfsg-3) unstable; urgency=medium

  * Team Upload.
  * Add python3-h5py to depends (Fixes debci)
  * Fix import-name
  * Fix lintian
  * Create manpages
  * compat version: 13

 -- Nilesh Patra <npatra974@gmail.com>  Sun, 19 Jul 2020 01:26:22 +0530

python-biom-format (2.1.8+dfsg-2) unstable; urgency=medium

  * Build-Depends: python3-matplotlib
    Closes: #955662
  * Standards-Version: 4.5.0 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Use secure URI in Homepage field.

 -- Andreas Tille <tille@debian.org>  Fri, 03 Apr 2020 23:18:06 +0200

python-biom-format (2.1.8+dfsg-1) unstable; urgency=medium

  * Drop cython from Build-Depends
    Closes: #937605
  * New upstream version
  * Set upstream metadata fields: Bug-Submit.

 -- Andreas Tille <tille@debian.org>  Mon, 20 Jan 2020 11:35:04 +0100

python-biom-format (2.1.7+dfsg-5) unstable; urgency=medium

  * Set upstream metadata fields: Bug-Database, Repository, Repository-
    Browse.
  * Fix Build time test
    Closes: #943622

 -- Andreas Tille <tille@debian.org>  Sun, 22 Dec 2019 07:51:11 +0100

python-biom-format (2.1.7+dfsg-4) unstable; urgency=medium

  [ Steve Langasek ]
  * Drop Python2 package
    Closes: #934864, #937605

  [ Andreas Tille ]
  * Fix doc-base control file
  * Standards-Version: 4.4.1
  * Set upstream metadata fields: Repository.

 -- Andreas Tille <tille@debian.org>  Fri, 08 Nov 2019 08:01:35 +0100

python-biom-format (2.1.7+dfsg-3) unstable; urgency=medium

  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * executable /usr/bin/biom in Python3 package
  * Examples in Python3 package to support testing
  * Do not copy build time tests to doc directory
    Closes: #919586
  * Drop biom-format-tools package - wrapper script is in Python3 package
  * Provide valid autopkgtest for Python3 module based on upstreams CI test
    Closes: #896953

 -- Andreas Tille <tille@debian.org>  Tue, 06 Aug 2019 10:11:48 +0200

python-biom-format (2.1.7+dfsg-2) unstable; urgency=medium

  * Do not ship *.c and *.pyx cython input files in binary packages
  * Fix "Could not import extension sphinx.ext.pngmath"
    Closes: #922258
  * Deactivate autopkgtest for the moment since it fails anyway

 -- Andreas Tille <tille@debian.org>  Wed, 13 Feb 2019 22:13:47 +0100

python-biom-format (2.1.7+dfsg-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.3.0
  * Provide data files for build-time test
  * Fix permissions
  * Secure URI in copyright format
  * Remove trailing whitespace in debian/changelog
  * rename debian/tests/control.autodep8 to debian/tests/control

 -- Andreas Tille <tille@debian.org>  Fri, 11 Jan 2019 23:50:29 +0100

python-biom-format (2.1.6+dfsg-2) unstable; urgency=medium

  * Force LC_ALL=C.UTF-8 in build and autopkgtest
    Closes: #896953, #901073
  * Drop ancient X-Python-Version

 -- Andreas Tille <tille@debian.org>  Thu, 21 Jun 2018 05:31:25 +0200

python-biom-format (2.1.6+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Fix build with sphinx 1.6
    Closes: #896629
  * cme fiy dpkg-control
     - Standards-Version: 4.1.4
     - Point Vcs-fields to Salsa
     - Crop superfluous Breaks/Replaces
  * debhelper 11
  * Build-Depends:
     - s/python-sphinx/python3-sphinx/
     - Added: python-pandas
  * Adapt doc-base files
  * Move d/t/control to d/t/control.autodep8
  * s/Testsuite: autopkgtest/Testsuite: autopkgtest-pkg-python/

 -- Andreas Tille <tille@debian.org>  Tue, 24 Apr 2018 18:27:20 +0200

python-biom-format (2.1.5+dfsg-7) unstable; urgency=medium

  * d/copyright: Update Upstream-Contact
  * Add another random seed to past tests relieably on all architectures

 -- Andreas Tille <tille@debian.org>  Thu, 12 Jan 2017 09:04:23 +0100

python-biom-format (2.1.5+dfsg-6) unstable; urgency=medium

  * Add random seed to test that fails under some circumstances
    Closes: #848410
  * debhelper 10
  * d/watch: version=4

 -- Andreas Tille <tille@debian.org>  Mon, 19 Dec 2016 22:29:00 +0100

python-biom-format (2.1.5+dfsg-5) unstable; urgency=medium

  * Restrict set of architectures also for biom-format-tools to make sure
    the package will not be built on architectures that are affected by
    the h5py issue.

 -- Andreas Tille <tille@debian.org>  Wed, 12 Oct 2016 11:09:10 +0200

python-biom-format (2.1.5+dfsg-4) unstable; urgency=medium

  * Restrict build to those architectures not affected by h5py issue
      https://github.com/h5py/h5py/issues/428
    This does not really close bug #820060 but we can lower its severity
    at least to important.

 -- Andreas Tille <tille@debian.org>  Mon, 12 Sep 2016 09:01:34 +0200

python-biom-format (2.1.5+dfsg-3) unstable; urgency=medium

  * Use sphinxdoc
    Closes: #831239
  * Use python-shinx in Build-Depends (instead only Build-Depends-Indep)
  * cme fix dpkg-control
  * hardening+=bindnow

 -- Andreas Tille <tille@debian.org>  Thu, 14 Jul 2016 14:04:56 +0200

python-biom-format (2.1.5+dfsg-2) unstable; urgency=medium

  * Breaks+Replaces: python-biom-format (<= 2.1+dfsg-1)
    Closes: #820028
  * Enable building arch all packages separately
    Closes: #820621

 -- Andreas Tille <tille@debian.org>  Sun, 10 Apr 2016 21:45:01 +0200

python-biom-format (2.1.5+dfsg-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Removed pyqi as well as generation of bash_completion
  * Build-Depends: python-click

  [ Ghislain Antony Vaillant ]
  * Properly run the upstream testsuite during the package build process
  * Add packaging testsuite
  * Provide documentation in separate arch-independent package
  * d/rules: add call to dh_numpy
  * let dh_python fill the binary package dependencies
  * Provide command-line tools in separate package
  * Provide the Python 3 package

 -- Andreas Tille <tille@debian.org>  Tue, 08 Mar 2016 15:38:30 +0100

python-biom-format (2.1.4+dfsg-2) unstable; urgency=medium

  * Fix test target
    Closes: #804441
  * Fix clean target enabling building twice in a row

 -- Andreas Tille <tille@debian.org>  Tue, 10 Nov 2015 22:35:15 +0100

python-biom-format (2.1.4+dfsg-1) unstable; urgency=medium

  * New upstream version
    Closes: #802867
  * Moved from SVN to Git
  * cme fix dpkg-control
  * Fix watch file

 -- Andreas Tille <tille@debian.org>  Tue, 27 Oct 2015 18:46:33 +0100

python-biom-format (2.1+dfsg-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 12 Aug 2014 14:43:54 +0200

python-biom-format (2.0.0+dfsg-1) unstable; urgency=medium

  * New upstream version (adpated Build-Depends and patches)
  * switch to dh-python using buildsystem=pybuild
  * New (versioned) Build-Depends
  * Add ${shlibs:Depends} since package is now arch any

 -- Andreas Tille <tille@debian.org>  Tue, 20 May 2014 19:16:23 +0200

python-biom-format (1.3.1+dfsg-1) unstable; urgency=medium

  [ Andreas Tille ]
  * New upstream version (updated patches).  New version also does not
    use Cython
    Closes: #742691
  * Package is now Architecture: all
  * Incorporate changes by BioLinux
  * debian/patches/no-web-adds.patch: Save user privacy by preventing inclusion
    of web adds
  * Do not install scripts printing "This script no longer exists. ..."
    any more
  * Use manpage created by sphinx

 -- Andreas Tille <tille@debian.org>  Thu, 27 Mar 2014 11:52:15 +0100

python-biom-format (1.2.0-1biolinux1) precise; urgency=low

  * Add runtime dep on PyQi
  * Patch /usr/bin/biom script to force /usr/lib/python2.7/dist-packages
    to the top of the PYTHONPATH
  * Allow building with cython 0.15.1 on Ubuntu as it builds and passes all
    the tests
  * Drop deal_with_none_array_metadata for now as it fails the self tests
    But it's still not right!
  * Rename add_metadata to something less generic.  It will die in the next
    release anyway.

 -- Tim Booth <tbooth@ceh.ac.uk>  Fri, 28 Feb 2014 16:25:41 +0000

python-biom-format (1.2.0-1) unstable; urgency=low

  * New upstream version
  * debian/control:
     - (Build-)Depends: pyqi
     - Build-Depends: python-nose to enable auto_test
  * debian/biom.1: Add simple manpage
  * debian/rules:
     - activate test using python-nose
     - deactivate get-orig-source script and use plain uscan since
       upstream tarball is clean now

 -- Andreas Tille <tille@debian.org>  Mon, 11 Nov 2013 09:24:46 +0100

python-biom-format (1.2.0-0biolinux2) precise; urgency=low

  * New upstream.
  * No need for the old cythonize tinkering in rules
  * Add examples to /usr/share/doc
  * Also add HTML API docs (single html build)
  * Build manpages from Sphinx
  * Add bash completion
  * Investigate and patch issue converting BIOM to table with
    taxonomy not set to an array

 -- Tim Booth <tbooth@ceh.ac.uk>  Wed, 25 Sep 2013 18:31:40 +0100

python-biom-format (1.1.2-1) unstable; urgency=low

  * debian/watch:
     - Enable '+dfsg' suffix in Debian versions
     - Fix download location
     - anonscm in Vcs fields
  * debian/control:
     - cme fix dpkg-control
  * Remove workaround cython problem which was needed when 1.0.0 was
    released because cython at this time was suffering from #682652
  * debian/rules:
     - use help2man to create manpages

 -- Andreas Tille <tille@debian.org>  Thu, 15 Aug 2013 01:17:43 +0200

python-biom-format (1.1.2-0ubuntu1) precise; urgency=low

  * New upstream.  Repack no longer needed.

 -- Tim Booth <tbooth@ceh.ac.uk>  Tue, 30 Apr 2013 18:09:17 +0100

python-biom-format (1.0.0+repack-1ubuntu1) precise; urgency=low

  * Rebuild for Precise
  * Minor changes to cleanup and get-orig-source that helped me

 -- Tim Booth <tbooth@ceh.ac.uk>  Fri, 10 Aug 2012 18:08:06 +0100

python-biom-format (1.0.0-1) unstable; urgency=low

  * Initial release (Closes: #682242)

 -- Andreas Tille <tille@debian.org>  Fri, 03 Aug 2012 10:38:26 +0200
